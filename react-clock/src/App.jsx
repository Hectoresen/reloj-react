import { DigitalClock, Countdown, Stopwatch } from './components';
import './App.scss';

function App() {
  return (
    <div className="App">
      <DigitalClock />
      <Countdown />
      <Stopwatch />
    </div>
  );
}

export default App;
