import React, { useEffect, useState } from "react";
import './digitalClock.scss';

const DigitalClock = () => {
    const [clockState, setClockState] = useState();

    useEffect(() =>{
        setInterval(() =>{
            const date = new Date();
            setClockState(date.toLocaleTimeString());
        }, 1000)
    }, []);
    return (
        <div className="clock-digital">
            <h2>Reloj digital</h2>
            <h1>🕰️ {clockState} 🕰️</h1>
        </div>
    );
};

export default DigitalClock