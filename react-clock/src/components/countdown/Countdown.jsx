import React, {useState, useEffect} from 'react';
import './countdown.scss'

const Countdown = () =>{
    const [time, setTime] = useState('');

    useEffect(() =>{
        let countDownDate = new Date("Apr 02, 2022 00:00:00").getTime();

        let y = setInterval(() =>{
            let now = new Date().getTime();
            let distance = countDownDate - now;
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            setTime(days + "d " + hours + "h " + minutes + "m " + seconds + "s");

            if(distance <0){
                clearInterval(y);
                setTime("Cuenta atrás finalizada");
            }
        }, 1000)
    }, []);

    return <div className='countdown'>
        <h2>Cuenta atrás</h2>
        <p>🏁 02/04/2022</p>
        <h1>{time}</h1>
        </div>
}

export default Countdown;